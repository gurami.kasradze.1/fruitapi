﻿using Core.Abstraction;
using Core.Enums;
using Core.Helpers.Responses;
using Core.Models;
using Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class TransferRequestRepository : ITransferRequestRepository
    {
        private readonly FruitStoreDbContext _dbContext;
        private readonly ITransferService _transferService;

        public TransferRequestRepository(FruitStoreDbContext dbContext, ITransferService transferService)
        {
            _dbContext = dbContext;
            _transferService = transferService;
        }

        public async Task<MasterResponse> ConfirmTransferRequest(int transferRequestId)
        {
            try
            {
                var tR = await _dbContext.TransferRequests.Include(i => i.Product).FirstOrDefaultAsync(i => i.TransferRequestId == transferRequestId);

                if (tR != default && tR.Status == TransferStatus.Pending)
                {
                    if (tR.Product.Stock < tR.Amount)
                        return new MasterResponse(StatusCodes.InsufficientStock);

                    tR.Status = TransferStatus.Accepted;

                    await _transferService.CreateTransfer(tR);

                    await _dbContext.SaveChangesAsync();

                    return new MasterResponse(StatusCodes.TransferRequestConfirmed);
                }

                return new MasterResponse(StatusCodes.TransferRequestNotFound);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<MasterResponse> CreateTransferRequest(decimal amount, int agentId, int productId)
        {
            try
            {
                var check = await _checkParams(amount, agentId, productId);

                if (check.StatusCode == StatusCodes.Ok)
                {
                    var product = await _dbContext.Products.FirstOrDefaultAsync(i => i.ProductId == productId);

                    if (product.Stock < amount)
                        return new MasterResponse(StatusCodes.InsufficientStock);

                    await _dbContext.TransferRequests.AddAsync(new TransferRequest
                    {
                        Amount = amount,
                        AgentId = agentId,
                        ProductId = productId,
                        CreatedAt = DateTime.Now,
                        Status = TransferStatus.Pending
                    });

                    await _dbContext.SaveChangesAsync();

                    return new MasterResponse(StatusCodes.TransferRequestCreated);
                }

                return check;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<MasterResponse> RejectTransferRequest(int transferRequestId)
        {
            try
            {
                var tR = await _dbContext.TransferRequests.FirstOrDefaultAsync(i => i.TransferRequestId == transferRequestId);

                if (tR != default)
                {
                    tR.Status = TransferStatus.Rejected;

                    await _dbContext.SaveChangesAsync();

                    return new MasterResponse(StatusCodes.TransferRequestRejected);
                }

                return new MasterResponse(StatusCodes.TransferRequestNotFound);
            }
            catch (Exception)
            {
                throw;
            }
        }

        async Task<MasterResponse> _checkParams(decimal amount, int agentId, int productId)
        {
            try
            {
                if (amount <= 0)
                    return new MasterResponse(StatusCodes.WrongAmount);

                var agent = await _dbContext.Agents.FirstOrDefaultAsync(i => i.AgentId == agentId);

                if (agent == default)
                    return new MasterResponse(StatusCodes.WrongAgentId);

                var product = await _dbContext.Products.FirstOrDefaultAsync(i => i.ProductId == productId);

                if (product == default)
                    return new MasterResponse(StatusCodes.WrongProductId);

                return new MasterResponse(StatusCodes.Ok);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
