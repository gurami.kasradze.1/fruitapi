﻿using Core.Abstraction;
using Core.Enums;
using Core.Helpers.Responses;
using Core.Models;
using Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class AgentRepository : IAgentRepository
    {
        private readonly FruitStoreDbContext _dbContext;

        public AgentRepository(FruitStoreDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<MasterResponse> CreateAgent(string firstName, string lastName, string privateId, int? managerId)
        {
            try
            {
                var check = await _checkParams(firstName, lastName, privateId, managerId);

                if (check.StatusCode == StatusCodes.Ok)
                {
                    await _dbContext.Agents.AddAsync(new Agent
                    {
                        FirstName = firstName,
                        LastName = lastName,
                        PrivateID = privateId,
                        ManagerId = managerId,
                        CreatedAt = DateTime.Now
                    });

                    await _dbContext.SaveChangesAsync();

                    return new MasterResponse(StatusCodes.AgentCreated);
                }

                return check;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<MasterResponse> DeleteAgent(string privateId)
        {
            try
            {
                var agent = await _dbContext.Agents.FirstOrDefaultAsync(i => i.PrivateID == privateId);

                if (agent != default)
                {
                    agent.DeletedAt = DateTime.Now;

                    await _dbContext.SaveChangesAsync();

                    return new MasterResponse(StatusCodes.AgentDeleted);
                }

                return new MasterResponse(StatusCodes.AgentNotFound);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<MasterResponse> GetAgentById(string privateId)
        {
            try
            {
                var agent = await _dbContext.Agents.FirstOrDefaultAsync(i => i.PrivateID == privateId);

                if (agent != default)
                {
                    return new Response<Agent>(StatusCodes.Ok, agent);
                }

                return new MasterResponse(StatusCodes.AgentNotFound);
            }
            catch (Exception)
            {
                throw;
            }
        }

        async Task<MasterResponse> _checkParams(string firstName, string lastName, string privateId, int? managerId)
        {
            if (string.IsNullOrEmpty(firstName))
                return new MasterResponse(StatusCodes.FirstNameEmpty);

            if (string.IsNullOrEmpty(lastName))
                return new MasterResponse(StatusCodes.LastNameEmpty);

            if (string.IsNullOrEmpty(privateId))
                return new MasterResponse(StatusCodes.PrivateIdEmpty);

            if (managerId.HasValue)
            {
                var mng = await _dbContext.Agents.FirstOrDefaultAsync(i => i.AgentId == managerId);

                if (mng == default)
                    return new MasterResponse(StatusCodes.WrongManagerId);
            }

            return new MasterResponse(StatusCodes.Ok);
        }
    }
}
