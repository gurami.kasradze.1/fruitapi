﻿using Core.Abstraction;
using Core.Enums;
using Core.Helpers.Responses;
using Core.Models;
using Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Services
{
    public class TransferService : ITransferService
    {
        private readonly FruitStoreDbContext _dbContext;

        public TransferService(FruitStoreDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<MasterResponse> CreateTransfer(TransferRequest model)
        {
            try
            {
                await _dbContext.Transfers.AddAsync(new Transfer
                {
                    AgentId = model.AgentId,
                    Price = model.Amount * model.Product.Price
                });

                await _dbContext.SaveChangesAsync();

                return new MasterResponse(StatusCodes.TransferCreated);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
