﻿using Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Data
{
    public class FruitStoreDbContext : DbContext
    {
        public DbSet<Agent> Agents { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<TransferRequest> TransferRequests { get; set; }
        public DbSet<Transfer> Transfers { get; set; }

        public FruitStoreDbContext(DbContextOptions<FruitStoreDbContext> options) : base(options) { }
    }
}
