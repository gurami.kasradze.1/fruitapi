﻿using Core.Abstraction;
using Core.Helpers.Responses;
using FruitStoreApi.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FruitStoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransferRequestsController : ControllerBase
    {
        private readonly ITransferRequestRepository _transferReqRepo;

        public TransferRequestsController(ITransferRequestRepository transferReqRepo)
        {
            _transferReqRepo = transferReqRepo;
        }

        [HttpPost("ConfirmTransferRequest")]
        public async Task<MasterResponse> ConfirmTransferRequest(int transferRequestId)
        {
            var response = await _transferReqRepo.ConfirmTransferRequest(transferRequestId);

            return response;
        }

        [HttpPost("CreateTransferRequest")]
        public async Task<MasterResponse> CreateTransferRequest(TransferRequestCreateDto model)
        {
            var response = await _transferReqRepo.CreateTransferRequest(model.Amount, model.AgentId, model.ProductId);

            return response;
        }

        [HttpPost("RejectTransferRequest")]
        public async Task<MasterResponse> RejectTransferRequest(int transferRequestId)
        {
            var response = await _transferReqRepo.RejectTransferRequest(transferRequestId);

            return response;
        }
    }
}
