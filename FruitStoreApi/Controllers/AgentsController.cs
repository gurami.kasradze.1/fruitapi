﻿using Core.Abstraction;
using Core.Helpers.Responses;
using Core.Models;
using FruitStoreApi.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FruitStoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AgentsController : ControllerBase
    {
        private readonly IAgentRepository _agentRepo;

        public AgentsController(IAgentRepository agentRepo)
        {
            _agentRepo = agentRepo;
        }

        [HttpPost("CreateAgent")]
        public async Task<MasterResponse> CreateAgent(AgentCreateDto model)
        {
            var response = await _agentRepo.CreateAgent(model.FirstName, model.LastName, model.PrivateID, model.ManagerId);

            return response;
        }

        [HttpPost("DeleteAgent")]
        public async Task<MasterResponse> DeleteAgent(string privateId)
        {
            var response = await _agentRepo.DeleteAgent(privateId);

            return response;
        }

        [HttpGet("GetAgentById")]
        public async Task<Response<Agent>> GetAgentById(string privateId)
        {
            var response = await _agentRepo.GetAgentById(privateId);

            return response as Response<Agent>;
        }
    }
}
