﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FruitStoreApi.DTOs
{
    public class AgentCreateDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PrivateID { get; set; }
        public int? ManagerId { get; set; }
    }
}
