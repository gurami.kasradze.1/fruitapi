﻿using Core.Enums;

namespace Core.Helpers.Responses
{
    public class Response<T> : MasterResponse
    {
        public T Data { get; set; }

        public Response(StatusCodes status, T data) : base(status)
        {
            Data = data;
        }
    }
}
