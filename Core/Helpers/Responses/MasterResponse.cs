﻿using Core.Enums;
using Core.Extensions;

namespace Core.Helpers.Responses
{
    public class MasterResponse
    {
        public MasterResponse(StatusCodes status)
        {
            StatusCode = status;
            Message = status.GetDisplayName();
        }

        public StatusCodes StatusCode { get; set; }

        public string Message { get; set; }
    }
}
