﻿using Core.Helpers.Responses;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Abstraction
{
    public interface IAgentRepository
    {
        Task<MasterResponse> CreateAgent(string firstName, string lastName, string privateId, int? managerId);
        Task<MasterResponse> GetAgentById(string privateId);
        Task<MasterResponse> DeleteAgent(string privateId);
    }
}
