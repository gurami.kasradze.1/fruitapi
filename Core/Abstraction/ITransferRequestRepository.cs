﻿using Core.Helpers.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Abstraction
{
    public interface ITransferRequestRepository
    {
        Task<MasterResponse> CreateTransferRequest(decimal amount, int agentId, int productId);
        Task<MasterResponse> ConfirmTransferRequest(int transferRequestId);
        Task<MasterResponse> RejectTransferRequest(int transferRequestId);
    }
}
