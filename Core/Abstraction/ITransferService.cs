﻿using Core.Helpers.Responses;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Abstraction
{
    public interface ITransferService
    {
        Task<MasterResponse> CreateTransfer(TransferRequest model);
    }
}
