﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Enums
{
    public enum StatusCodes
    {
        [Display(Name = "წარმატებული")]
        Ok,
        [Display(Name = "არასწორი რაოდენობა")]
        WrongAmount,
        [Display(Name = "არასწორი აგენტის აიდი")]
        WrongAgentId,
        [Display(Name = "არასწორი პროდუქტის აიდი")]
        WrongProductId,
        [Display(Name = "ტრანსფერის მოთხოვნა შეიქმნა")]
        TransferRequestCreated,
        [Display(Name = "ტრანსფერის მოთხოვნა დადასტურდა")]
        TransferRequestConfirmed,
        [Display(Name = "ტრანსფერის მოთხოვნა უარყოფილია")]
        TransferRequestRejected,
        [Display(Name = "ტრანსფერის მოთხოვნა არ მოიძებნა")]
        TransferRequestNotFound,
        [Display(Name = "მარაგი არ არის საკმარისი")]
        InsufficientStock,
        [Display(Name = "აგენტი შეიქმნა")]
        AgentCreated,
        [Display(Name = "სახელი ცარიელია")]
        FirstNameEmpty,
        [Display(Name = "გვარი ცარიელია")]
        LastNameEmpty,
        [Display(Name = "პირადი ნომერი ცარიელია")]
        PrivateIdEmpty,
        [Display(Name = "მენეჯერის id არასწორია")]
        WrongManagerId,
        [Display(Name = "აგენტი წაიშალა")]
        AgentDeleted,
        [Display(Name = "აგენტი არ მოიძებნა")]
        AgentNotFound,
        [Display(Name = "ტრანსფერი შეიქმნა")]
        TransferCreated,
        [Display(Name = "ზოგადი შეცდომა")]
        GeneralError
    }
}
